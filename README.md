# N-Queen Problem _(n-queen\_problem)_

The N-Queen Problem is the problem of placing N chess queens on a NxN board without any of them threatening eachother. This program attempts to do so for an arbitrarily large N.

Makes a grid of size n and places n queens such that no queen can be attacked by another.

## Install
To run the script you need to have Python3 installed. If you are on windows get Python3 from windows store or use (Anaconda)[www.anaconda.com]. On linux Python3 should come with the basic system installation.

## Usage
To run the script type `python3 nQueenRec.py` in a terminal or Cmd. Once finished the script reveals all the solutions for an 8x8 board.

## Contributing
Send questions and suggestions to <thomas.haaland@gmail.com>

UNLICENSED