import copy

"""
This class finds all solutions to the Nqueen problem.
"""
class NQueen:
    # Initiates class by making the board and reserves a place for keeping the solutions
    def __init__(self, n):
        self.n = n
        self.nQ = 0
        self.board = [['0' for j in range(n)] for i in range(n)]
        self.goodBoards = []

    # Method for assessing attacks in square
    def assessAttacks(self, x, y):
        #counter = 0
        posDiag = x - y
        negDiag = x + y
        for i in range(self.n):
            if self.board[i][y] == 'Q':
                return False
            pdy = i - posDiag
            if (pdy >= 0 and pdy < self.n and self.board[i][pdy] == 'Q'):
                return False
            ndy = negDiag - i
            if (ndy >= 0 and ndy < self.n and self.board[i][ndy] == 'Q'):
                return False
        return True
        
    # Method for placing the queen
    def placeQueen(self, i):
        if i >= self.n:
            if self.nQ == self.n:
                self.goodBoards.append(copy.deepcopy(self.board))
            else:
                return False
        for j in range(self.n):
            if self.assessAttacks(i,j):# == 0:
                self.board[i][j] = 'Q'
                self.nQ += 1
                if self.placeQueen(i+1):
                    return True
                self.board[i][j] = '0'
                self.nQ -= 1
        return False

    # Prints all boards
    def printBoards(self):
        n = self.n
        for board in self.goodBoards:
            for i in range(n):
                for j in range(n):
                    print(board[i][j], end = ' ')
                print()
            print()

    # Method for initating the whole thing  
    def findSolution(self):
        n = self.n
        return self.placeQueen(0)



def main():
    queenGame = NQueen(8)
    queenGame.findSolution()
    print("All solutions: ")
    queenGame.printBoards()
if __name__ == "__main__":
    main()